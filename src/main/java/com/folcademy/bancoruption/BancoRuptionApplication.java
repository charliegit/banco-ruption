package com.folcademy.bancoruption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoRuptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoRuptionApplication.class, args);
	}

}
