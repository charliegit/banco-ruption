package com.folcademy.bancoruption.controllers;




import com.folcademy.bancoruption.models.entities.dto.NewTransactionDTO;
import com.folcademy.bancoruption.models.entities.dto.TransactionDetailedDTO;
import com.folcademy.bancoruption.models.entities.dto.TransactionsDTO;

import com.folcademy.bancoruption.services.TransactionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {

    private final TransactionsService transactionsService;

    public TransactionsController(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }


    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTransactionsForAccount(@PathVariable Long accountNumber) {

        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }
    @GetMapping("/{transactionId}")
    public ResponseEntity<TransactionDetailedDTO>getTransaction(@PathVariable Long transactionId)
    {
        return new ResponseEntity<>(transactionsService.getTransaction(transactionId), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO) {
        return transactionsService.createTransaction(newTransactionDTO);


    }
}