package com.folcademy.bancoruption.controllers;

import com.folcademy.bancoruption.models.entities.dto.AccountsDTO;
import com.folcademy.bancoruption.models.entities.dto.NewAccountDTO;
import com.folcademy.bancoruption.services.AccountsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")



public class AccountsController {
    private final AccountsService accountsService;

    public AccountsController(AccountsService accountsService) {
        this.accountsService = accountsService;
    }


    @GetMapping("/{userId}")
    public ResponseEntity<AccountsDTO> getUserAccounts(@PathVariable String userId) {

        return new ResponseEntity<>(accountsService.getUsers(userId), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> createAccount(@RequestBody NewAccountDTO newAccountDTO) {
        return accountsService.createAccount(newAccountDTO);
    }
}

    /*  @PostMapping("/new")
    public ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO) {
        return transactionsService.createTransaction(newTransactionDTO);

*/
