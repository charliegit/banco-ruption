package com.folcademy.bancoruption.controllers;

import com.folcademy.bancoruption.exceptions.NotAuthorizedException;
import com.folcademy.bancoruption.exceptions.NotFoundException;
import com.folcademy.bancoruption.models.entities.dto.AuthenticateRequest;
import com.folcademy.bancoruption.models.entities.dto.AuthenticationResponse;
import com.folcademy.bancoruption.security.JwtUtil;
import org.apache.catalina.Authenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class AutenthicateController {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/authenticate")
    public ResponseEntity< AuthenticationResponse >createToken(@RequestBody AuthenticateRequest authenticateRequest){
        UserDetails userDetails;
        try {
            userDetails =userDetailsService.loadUserByUsername(authenticateRequest.getUsername());
        } catch (UsernameNotFoundException e){
            throw new NotAuthorizedException("Username or password are incorrect");
        }

        if (Objects.isNull(userDetails)|| userDetails.getPassword().equals(authenticateRequest.getPassword())){
            throw  new NotAuthorizedException("El User o la Contraseña son incorrectos");
        }
        authenticationManager.authenticate (
             new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(),authenticateRequest.getPassword())
        );

    String jwt= "Bearer " + jwtUtil.generateToken(userDetails);
        AuthenticationResponse response = new AuthenticationResponse(jwt);
        return ResponseEntity.ok(response);
    }
}
