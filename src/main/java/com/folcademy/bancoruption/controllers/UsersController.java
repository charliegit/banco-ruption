package com.folcademy.bancoruption.controllers;

import com.folcademy.bancoruption.models.entities.UserEntity;
import com.folcademy.bancoruption.models.entities.dto.NewUserDTO;
import com.folcademy.bancoruption.models.entities.dto.UsersDTO;
import com.folcademy.bancoruption.models.entities.repositories.UserRepository;
import com.folcademy.bancoruption.services.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UsersController {
    private final UserRepository userRepository;
    private final UsersService usersService;

    public UsersController(UserRepository userRepository, UsersService usersService) {
        this.userRepository = userRepository;
        this.usersService = usersService;
    }



    @GetMapping("/{dni}")
    public ResponseEntity<UsersDTO> getUsuarios(@PathVariable String dni) {

        return new ResponseEntity<>(usersService.getUsers(dni),HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> createUser(@RequestBody NewUserDTO newUserDTO){

        return usersService.createUser(newUserDTO);
    }

}
