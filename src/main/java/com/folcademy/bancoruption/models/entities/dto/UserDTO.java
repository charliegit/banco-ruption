package com.folcademy.bancoruption.models.entities.dto;

public class UserDTO {
    private String dni;
    private String firstname;
    private String lastname;
    private String address;

    public UserDTO(String dni, String firstname, String lastname, String address) {
        this.dni = dni;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }
}
