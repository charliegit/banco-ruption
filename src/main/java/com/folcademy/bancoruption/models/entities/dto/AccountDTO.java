package com.folcademy.bancoruption.models.entities.dto;

public class AccountDTO {
    private String firstname;
    private String lastname;
    private String cbu;

    public AccountDTO(String firstname, String lastname, String cbu) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.cbu = cbu;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCbu() {
        return cbu;
    }
}
