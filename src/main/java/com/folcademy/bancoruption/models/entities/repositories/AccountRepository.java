package com.folcademy.bancoruption.models.entities.repositories;

import com.folcademy.bancoruption.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<AccountEntity,Long> {
        List<AccountEntity> findAllByUserId(String userId);
        AccountEntity findByNumber(Long number);
}
