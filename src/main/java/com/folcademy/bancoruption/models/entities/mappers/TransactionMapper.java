package com.folcademy.bancoruption.models.entities.mappers;

import com.folcademy.bancoruption.models.entities.TransactionsEntity;
import com.folcademy.bancoruption.models.entities.dto.NewTransactionDTO;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class TransactionMapper {
    public TransactionsEntity mapTransactionDtoToTransactionEntity(NewTransactionDTO newTransactionDTO) {
        return new TransactionsEntity(new Date(),
                newTransactionDTO.getDescription(),
                newTransactionDTO.getAmount(),
                newTransactionDTO.getCurrency(),
                newTransactionDTO.getFrom(),
                newTransactionDTO.getTo());

        }
    }

