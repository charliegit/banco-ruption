package com.folcademy.bancoruption.models.entities.dto;



public class NewAccountDTO {

    private Long number;
    private String cbu;
    private String type;
    private String userId;

    public NewAccountDTO(Long number, String cbu, String type, String userId) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userId = userId;
    }


    public Long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public String getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }
}
