package com.folcademy.bancoruption.models.entities.dto;

import java.util.List;

public class AccountsDTO {
    private List<AccountDTOforAccountController> accounts;

    public AccountsDTO(List<AccountDTOforAccountController> accounts) {
        this.accounts = accounts;
    }

    public List<AccountDTOforAccountController> getAccounts() {
        return accounts;
    }
}






