package com.folcademy.bancoruption.models.entities.dto;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionDetailedDTO {
    private String description;
    private BigDecimal amount;
    private String currency;
    private AccountDTO from;
    private AccountDTO to;
    private Date date;

    public TransactionDetailedDTO(String description, BigDecimal amount, String currency, AccountDTO from, AccountDTO to,Date date) {

        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.date = date;
    }



    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountDTO getFrom() {
        return from;
    }

    public AccountDTO getTo() {
        return to;
    }
}
