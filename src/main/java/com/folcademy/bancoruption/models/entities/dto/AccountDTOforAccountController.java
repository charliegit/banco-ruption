package com.folcademy.bancoruption.models.entities.dto;


public class AccountDTOforAccountController {

    private Long numberaccount;
    private String accountcbu;
    private String typeaccount;
    private String userdni;


    public AccountDTOforAccountController(Long numberaccount, String accountcbu, String typeaccount, String userdni){
        this.numberaccount = numberaccount;
        this.accountcbu = accountcbu;
        this.typeaccount = typeaccount;
        this.userdni = userdni;
    }


    public Long getNumberaccount() {
        return numberaccount;
    }

    public String getAccountcbu() {
        return accountcbu;
    }

    public String getTypeaccount() {
        return typeaccount;
    }

    public String getUserdni() {
        return userdni;
    }
}

