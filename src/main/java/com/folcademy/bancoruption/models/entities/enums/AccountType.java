package com.folcademy.bancoruption.models.entities.enums;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}
