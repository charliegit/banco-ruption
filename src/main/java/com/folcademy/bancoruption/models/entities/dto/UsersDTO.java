package com.folcademy.bancoruption.models.entities.dto;

import java.util.List;

public class UsersDTO {
    private List<UserDTO> users;


    public UsersDTO() {
    }

    public UsersDTO(List<UserDTO> users) {
        this.users = users;
    }

    public List<UserDTO> getUsers() {
        return users;
    }
}
