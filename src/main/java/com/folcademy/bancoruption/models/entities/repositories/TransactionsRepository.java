package com.folcademy.bancoruption.models.entities.repositories;

import com.folcademy.bancoruption.models.entities.TransactionsEntity;
import org.hibernate.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionsRepository extends CrudRepository<TransactionsEntity,Long> {
    List<TransactionsEntity> findAllByOriginOrDestination(String origin,String destination);
     Optional <TransactionsEntity> findById(Long id);
}
