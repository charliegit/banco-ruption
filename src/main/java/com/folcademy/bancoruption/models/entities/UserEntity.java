package com.folcademy.bancoruption.models.entities;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {

    @Id
    private String dni;
    private String firstname;
    private String lastname;
    private String address;
    private String username;
    private String password;

    public UserEntity() {
    }

    public UserEntity(String dni, String firstname, String lastname, String address, String password) {
        this.dni = dni;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.password = password;
    }

    public UserEntity(String dni, String firstname, String lastname, String address) {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }

    public String getFullName()
    {
        return firstname + " " +lastname;
    }
}