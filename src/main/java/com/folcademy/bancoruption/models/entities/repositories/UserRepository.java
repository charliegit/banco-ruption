package com.folcademy.bancoruption.models.entities.repositories;

import com.folcademy.bancoruption.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity,Long> {
          
            UserEntity findByDni(String dni);

    List<UserEntity> findAllByDni(String dni);
    Optional<UserEntity> findByUsername(String username);
}
