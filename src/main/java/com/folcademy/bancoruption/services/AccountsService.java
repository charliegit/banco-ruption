package com.folcademy.bancoruption.services;

import com.folcademy.bancoruption.models.entities.AccountEntity;


import com.folcademy.bancoruption.models.entities.UserEntity;
import com.folcademy.bancoruption.models.entities.dto.AccountDTOforAccountController;

import com.folcademy.bancoruption.models.entities.dto.AccountsDTO;
import com.folcademy.bancoruption.models.entities.dto.NewAccountDTO;
import com.folcademy.bancoruption.models.entities.dto.NewUserDTO;
import com.folcademy.bancoruption.models.entities.repositories.AccountRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AccountsService {

    private final AccountRepository accountRepository;


    public AccountsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }




    public AccountsDTO getUsers(String id) {
        List<AccountEntity> accountetities = accountRepository.findAllByUserId(id);
        List<AccountDTOforAccountController> accountDTOList = new ArrayList<>();

        for (AccountEntity entity : accountetities) {
            accountDTOList.add(
                    new AccountDTOforAccountController(entity.getNumber(),
                            entity.getCbu(),
                            entity.getType().toString(),
                            entity.getUserId())
            );
        }
        return new AccountsDTO(accountDTOList);


    }
    public ResponseEntity<String> createAccount(NewAccountDTO newAccountDTO) {
    AccountEntity toAccountEntity;
        toAccountEntity =accountRepository.findByNumber(newAccountDTO.getNumber());
        if (Objects.nonNull(toAccountEntity))
        {
            return new ResponseEntity<>("Ya existe el usuario registrado", HttpStatus.NOT_FOUND);


        }AccountEntity entidadNueva =new AccountEntity (newAccountDTO.getNumber(),newAccountDTO.getCbu(), newAccountDTO.getType(), newAccountDTO.getUserId());


        accountRepository.save(entidadNueva);
        return new ResponseEntity<>("Transaccion Creada!",HttpStatus.OK);
    }


}
