package com.folcademy.bancoruption.services;


import com.folcademy.bancoruption.exceptions.BadRequestException;
import com.folcademy.bancoruption.exceptions.NotFoundException;
import com.folcademy.bancoruption.models.entities.AccountEntity;
import com.folcademy.bancoruption.models.entities.TransactionsEntity;
import com.folcademy.bancoruption.models.entities.UserEntity;
import com.folcademy.bancoruption.models.entities.dto.*;
import com.folcademy.bancoruption.models.entities.mappers.TransactionMapper;
import com.folcademy.bancoruption.models.entities.repositories.AccountRepository;
import com.folcademy.bancoruption.models.entities.repositories.TransactionsRepository;
import com.folcademy.bancoruption.models.entities.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TransactionsService {
    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final TransactionMapper transactionMapper;


    public TransactionsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository userRepository, TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionMapper = transactionMapper;

    }

    public TransactionsDTO getTransactions(Long accountNumber) {
        List<TransactionsEntity> transactionsEntities = transactionsRepository.findAllByOriginOrDestination(accountNumber.toString(), accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();


        for (TransactionsEntity transactionsEntity : transactionsEntities) {
            AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
            UserEntity toUserEntity = userRepository.findByDni(toAccountEntity.getUserId());
            String transactionType;

            if (transactionsEntity.getOrigin().equals(accountNumber.toString())) {
                transactionType = "GASTO";
            } else {
                transactionType = "INGRESO";
            }

            transactionDTOList.add(new TransactionDTO(transactionsEntity.getDate().toString(),
                    transactionsEntity.getDescription(),
                    transactionsEntity.getAmount(),
                    transactionsEntity.getCurrency().toString(),
                    transactionsEntity.getOrigin(),
                    toUserEntity.getFullName() + "/CBU: " + toAccountEntity.getCbu(),
                    transactionType)
            );
        }
        BigDecimal balance = BigDecimal.ZERO;
        for (TransactionDTO transactionDTO : transactionDTOList) {
            if (transactionDTO.getType().equals("GASTO")) {
                balance = balance.subtract(transactionDTO.getAmount());
            } else {
                balance = balance.add(transactionDTO.getAmount());
            }
        }
        return new TransactionsDTO(transactionDTOList, balance);
    }


    public TransactionDetailedDTO getTransaction(long id) {

        Optional<TransactionsEntity> transactionsEntityOptional = transactionsRepository.findById(id);
        if (transactionsEntityOptional.isEmpty()) {

            throw new NotFoundException("No existe la transaccion");
        }
        TransactionsEntity transactionsEntity = transactionsEntityOptional.get();
        AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
        AccountEntity fromAccountEntity =accountRepository.findByNumber(Long.parseLong(transactionsEntity.getOrigin()));
        UserEntity toUserEntity = userRepository.findByDni(toAccountEntity.getUserId());
        UserEntity fromUserEntity = userRepository.findByDni(fromAccountEntity.getUserId());
        AccountDTO to = new AccountDTO(toUserEntity.getFirstname(), toUserEntity.getLastname(), toAccountEntity.getCbu());
        AccountDTO from = new AccountDTO(fromUserEntity.getFirstname(), fromUserEntity.getLastname(), fromAccountEntity.getCbu());
        TransactionDetailedDTO transactionDetailedDTO = new TransactionDetailedDTO(transactionsEntity.getDescription(),
                transactionsEntity.getAmount(),
                transactionsEntity.getCurrency(),
                to,
                from,
                transactionsEntity.getDate());

        return transactionDetailedDTO;

    }



    public ResponseEntity<String> createTransaction(NewTransactionDTO newTransactionDTO) {
     AccountEntity toAccountEntity=accountRepository.findByNumber(Long.parseLong(newTransactionDTO.getTo()));
     if(newTransactionDTO.getTo().equals(newTransactionDTO.getFrom()))
        {
            throw  new BadRequestException("No se puede enviar dinero hacia la misma cuenta");
        }
        if (Objects.isNull(toAccountEntity))
        {
            throw new NotFoundException("No existe la cuenta solicitada");

        }
        if(newTransactionDTO.getAmount().intValue()<=0)
        {
            throw  new BadRequestException("El monto es negativo o no cero");
        }
        TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
        transactionsRepository.save(transactionsEntity);
        return new ResponseEntity<>("Transaccion Creada!",HttpStatus.OK);
    }


        /*        try {
            TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
            transactionsRepository.save(transactionsEntity);
            return new ResponseEntity<>("Transaccion Creada", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Algo salió mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }*/


}
