package com.folcademy.bancoruption.services;



import com.folcademy.bancoruption.models.entities.UserEntity;
import com.folcademy.bancoruption.models.entities.dto.NewUserDTO;
import com.folcademy.bancoruption.models.entities.dto.UserDTO;
import com.folcademy.bancoruption.models.entities.dto.UsersDTO;
import com.folcademy.bancoruption.models.entities.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UsersService {
    private final UserRepository userRepository;

    public UsersService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UsersDTO getUsers(String dni) {
        List<UserEntity> userEntities = userRepository.findAllByDni(dni);
        List<UserDTO> userDTOList = new ArrayList<>();

        for (UserEntity entity : userEntities) {
            userDTOList.add(
                    new UserDTO(entity.getDni(),
                            entity.getFirstname(),
                            entity.getLastname(),
                            entity.getAddress())
            );
        }
        return new UsersDTO(userDTOList);
    }




    public ResponseEntity<String> createUser(NewUserDTO newUserDTO) {
       UserEntity toUserEntity;
        toUserEntity =userRepository.findByDni(newUserDTO.getDni());
            if (Objects.nonNull(toUserEntity))
        {
            return new ResponseEntity<>("Ya existe el usuario registrado",HttpStatus.NOT_FOUND);


        }UserEntity entidadNueva =new UserEntity ( newUserDTO.getDni(),newUserDTO.getFirstname(), newUserDTO.getLastname(), newUserDTO.getAddress());


        userRepository.save(entidadNueva);
        return new ResponseEntity<>("Transaccion Creada!",HttpStatus.OK);
    }




}
